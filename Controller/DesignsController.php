<?php
App::uses('AppController', 'Controller');
/**
 * Designs Controller
 *
 * @property Design $Design
 * @property PaginatorComponent $Paginator
 */
class DesignsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Design->recursive = 0;
		$this->set('designs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
		$this->set('design', $this->Design->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Design->create();
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		}
		$themes = $this->Design->Theme->find('list');
		$categories = $this->Design->Category->find('list');
		$this->set(compact('themes', 'categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
			$this->request->data = $this->Design->find('first', $options);
		}
		$themes = $this->Design->Theme->find('list');
		$categories = $this->Design->Category->find('list');
		$this->set(compact('themes', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Design->id = $id;
		if (!$this->Design->exists()) {
			throw new NotFoundException(__('Invalid design'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Design->delete()) {
			$this->Session->setFlash(__('The design has been deleted.'));
		} else {
			$this->Session->setFlash(__('The design could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
