<div class="designs form">
<?php echo $this->Form->create('Design'); ?>
	<fieldset>
		<legend><?php echo __('Edit Design'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('theme_id');
		echo $this->Form->input('title');
		echo $this->Form->input('story');
		echo $this->Form->input('notes');
		echo $this->Form->input('category_id');
		echo $this->Form->input('designs_tags');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Design.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Design.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Designs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Themes'), array('controller' => 'themes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Theme'), array('controller' => 'themes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Votes'), array('controller' => 'votes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vote'), array('controller' => 'votes', 'action' => 'add')); ?> </li>
	</ul>
</div>
