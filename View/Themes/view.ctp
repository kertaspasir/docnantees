<div class="themes view">
<h2><?php echo __('Theme'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($theme['Theme']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($theme['Theme']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Theme'), array('action' => 'edit', $theme['Theme']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Theme'), array('action' => 'delete', $theme['Theme']['id']), null, __('Are you sure you want to delete # %s?', $theme['Theme']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Themes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Theme'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Designs'), array('controller' => 'designs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Design'), array('controller' => 'designs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Designs'); ?></h3>
	<?php if (!empty($theme['Design'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Theme Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Story'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Designs Tags'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($theme['Design'] as $design): ?>
		<tr>
			<td><?php echo $design['id']; ?></td>
			<td><?php echo $design['theme_id']; ?></td>
			<td><?php echo $design['title']; ?></td>
			<td><?php echo $design['story']; ?></td>
			<td><?php echo $design['notes']; ?></td>
			<td><?php echo $design['category_id']; ?></td>
			<td><?php echo $design['designs_tags']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'designs', 'action' => 'view', $design['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'designs', 'action' => 'edit', $design['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'designs', 'action' => 'delete', $design['id']), null, __('Are you sure you want to delete # %s?', $design['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Design'), array('controller' => 'designs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
