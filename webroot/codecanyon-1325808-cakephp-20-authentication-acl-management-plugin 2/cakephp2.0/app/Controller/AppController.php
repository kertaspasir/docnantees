<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
            'Acl',
            'Auth' => array(
                'authenticate' => array(
                    'Form' => array(
                        'fields' => array('username' => 'email'),
                        'scope'  => array('User.status' => 1)
                    )
                ),
                'authorize' => array(
                    'Actions' => array('actionPath' => 'controllers')
                )
            ),
            'Session'
        );

    public $helpers = array(
        'Session',
        'Form',
        'Html',
        'Cache',
        'Js',
        'Time'
    );
    public function beforeFilter() {
        parent::beforeFilter();
		
        //$this->Auth->allow("*");//comment after generate action
		
        //Configure AuthComponent
        $this->Auth->flash = array("element"=>"error", "key"=>"auth", "params"=>array());
        $this->Auth->loginAction = '/users/login';
        $this->Auth->logoutRedirect = '/users/login';
        $this->Auth->loginRedirect = array('plugin'=>false, 'controller' => 'posts', 'action' => 'index');    }

}

?>